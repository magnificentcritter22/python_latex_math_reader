import re

DOCUMENT_BODY_PATTERN = {
    'pattern': r'\\begin{document}.*\\end{document}',
    'flags': re.DOTALL,
}

INLINE_MATH_PATTERNS = [
    {
        'pattern': r'\$.*?\$',
        'flags': re.DOTALL,
    },
    {
        'pattern': r'\\\(.*?\\\)',
        'flags': re.DOTALL,
    },
    {
        'pattern': r'\\begin{equation}.*?\\end{equation}',
        'flags': re.DOTALL,
    }
]

DISPLAY_MATH_PATTERNS = [
    {
        'pattern': r'\${2}.*?\${2}',
        'flags': re.DOTALL,
    },
    {
        'pattern': r'\\\[.*?\\\]',
        'flags': re.DOTALL,
    },
    {
        'pattern': r'\\begin{displaymath}.*?\\end{displaymath}',
        'flags': re.DOTALL,
    },
    {
        'pattern': r'\\begin{eqnarray\*?}.*?\\end{eqnarray\*?}',
        'flags': re.DOTALL,
    }
]