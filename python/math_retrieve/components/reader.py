'''
A LaTex file reader abstract module
'''
from typing import Optional, TextIO
from tkinter.filedialog import askopenfilename


class LaTeXReader:
    def __init__(
            self,
            filename: Optional[str] = None,
            use_opendialog: Optional[bool] = False,
            *args,
            **kwargs,
        ) -> None:
        super().__init__(*args, **kwargs)
        self.file = None
        if use_opendialog:
            filename = askopenfilename(
                defaultextension='*.tex',
            )
            if filename:
                self.filename = filename
                return
            else:
                print('No file chosen')
                exit()
        elif filename:
            self.filename = filename
            return
        else:
            raise TypeError('constructor requires either filename provided or dialog window opened')
    
    def open(self) -> TextIO:
        self.file = open(self.filename, 'r', encoding='utf-8')
        return self.file
    
    def __enter__(self):
        return self.open()
    
    def __exit__(self, *args, **kwargs) -> None:
        if args:
            return False
        self.file.close()
    