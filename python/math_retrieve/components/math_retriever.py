from typing import List

import re
from re import Match
from .constants.patterns import (
    DOCUMENT_BODY_PATTERN,
    INLINE_MATH_PATTERNS,
    DISPLAY_MATH_PATTERNS,
)
from .math_object.math_object import LaTeXMathObject


class LaTeXMathRetriever:
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)
        self.formula_objects = []

    def pattern_retrieval(self):
        if not self.file:
            raise FileNotFoundError('Open file first')
        filecontent = self.file.read()
        body = self._get_document_content(filecontent)
        matches = self._match_math(body)
        for match in matches:
            if len(match.group(0)) >2:
                self.formula_objects.append(
                    LaTeXMathObject(match)
                )
        print(*self.formula_objects, sep='\n')


    def get_document_content(self) -> str:
        return self._get_document_content(
            self.file.read()
        )
    
    @staticmethod
    def _get_document_content(string: str) -> str:
        body = re.search(
            string=string,
            **DOCUMENT_BODY_PATTERN
        )
        if not body:
            raise ValueError('Could not find document body. Invalid file')
        
        return body.group(0)

    
    @staticmethod
    def _match_math(string: str) -> List[Match]:
        result_matches = []
        for patterns in [INLINE_MATH_PATTERNS, DISPLAY_MATH_PATTERNS]:
            for pattern in patterns:
                matches = re.finditer(
                    string=string,
                    **pattern,
                )
                if matches:
                    for match in matches:
                        result_matches.append(match)
        
        return result_matches


    