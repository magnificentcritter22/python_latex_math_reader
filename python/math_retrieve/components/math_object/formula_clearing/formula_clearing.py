import re


class FormulaClearer:

    @staticmethod
    def _clear_formula(math_string: str) -> str:
        cleared_delimeters = FormulaClearer._clear_latex_delimiters(math_string)
        cleared_newlines = FormulaClearer._clear_newlines(cleared_delimeters)
        cleared_operator_whitespaces = FormulaClearer._unify_operators_whitespaces(cleared_newlines)
        spaces_stripped = cleared_operator_whitespaces.strip()
        return spaces_stripped

    @staticmethod
    def _clear_latex_delimiters(math_string: str) -> str:
        # identifying with no regex
        if math_string[0] == r'$' and math_string[:2] != r'$$':
            return math_string[1:-1]
        
        if math_string[:2] == r'\(' or math_string[:2] == r'$$' or math_string[:2] == r'\[':
            return math_string[2:-2]

        if math_string.startswith(r'\begin{math}'):
            return math_string[
                len(r'\begin{math}'):-len(r'\end{math}')
            ]
        
        if math_string.startswith(r'\begin{equation}'):
            return math_string[
                len(r'\begin{equation}'):-len(r'\end{equation}')
            ]
        
        if math_string.startswith(r'\begin{eqnarray}'):
            return math_string[
                len(r'\begin{eqnarray}'):-len(r'\end{eqnarray}')
            ]
        
        
        if math_string.startswith(r'\begin{eqnarray*}'):
            return math_string[
                len(r'\begin{eqnarray*}'):-len(r'\end{eqnarray*}')
            ]

    @staticmethod
    def _clear_newlines(math_string: str) -> str:
        return math_string.replace('\n', '')
    
    @staticmethod
    def _unify_operators_whitespaces(math_string: str) -> str:
        add_operator = re.sub(r'\s*\+\s*', ' + ', math_string)
        sub_operator = re.sub(r'\s*\-\s*', ' - ', add_operator)
        eq_operator = re.sub(r'\s*\=\s*', ' = ', sub_operator)
        return eq_operator