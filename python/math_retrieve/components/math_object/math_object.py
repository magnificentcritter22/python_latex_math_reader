from re import Match
from .formula_clearing import FormulaClearer

class LaTeXMathObject(FormulaClearer):
    def __init__(self, math_string: Match) -> None:
        self._init_value = math_string.group(0)
        self.starts_at = math_string.start()
        self.ends_at = math_string.end()
        self._clean_formula = self._clear_formula(self._init_value)


    def __str__(self) -> str:
        return f'[{self.starts_at: >5} - {self.ends_at: >5} : ({self.ends_at - self.starts_at: >5})]: {self._clean_formula}'