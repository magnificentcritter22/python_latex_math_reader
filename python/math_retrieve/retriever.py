from .components import (
    LaTeXReader,
    LaTeXMathRetriever
)

class LaTeXFormulaRetriever(
    LaTeXReader,
    LaTeXMathRetriever,
):
    def __init__(self, filename: str | None = None, use_opendialog: bool | None = False) -> None:
        super().__init__(filename, use_opendialog)