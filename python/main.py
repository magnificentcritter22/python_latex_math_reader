from math_retrieve.retriever import LaTeXFormulaRetriever

if __name__ == '__main__':
    filereader = LaTeXFormulaRetriever(use_opendialog=True)
    filereader.open()
    filereader.pattern_retrieval()